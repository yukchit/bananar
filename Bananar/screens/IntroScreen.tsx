import React, {useEffect} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {useNavigation, StackActions} from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function IntroScreen() {

  const navigation = useNavigation();

  return (
    <View style={styles.body}>
      <View style={styles.safeAreaViewContainer}>
        <Image
          style={styles.logo}
          source={require('../images/banana-nutrition-snip-psed.png')}
        />
        <View style={styles.backButtonContainer}>
            <TouchableOpacity
                style={styles.backButton}
                onPress={() => navigation.navigate('Welcome')}>
                <Image
                    style={styles.backLogo}
                    source={require('../icons/back-arrow-128.png')}
                />
            </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  body: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: 'white',
  },
  safeAreaViewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  logo: {
    width: 340,
    height: 270,
    marginBottom: 5,
  },
  backButtonContainer: {
    marginTop: 50,
    height: 50,
    display: 'flex',
    justifyContent: 'center',
  },
  loadingText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 15,
  },
  backButton: {

  },
  backLogo: {
    height: 50,
    width: 50
  }
});
