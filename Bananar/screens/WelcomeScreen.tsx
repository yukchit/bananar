import React, {useEffect} from 'react';
import {
  View,
  StyleSheet,
  Text,
  ImageBackground,
  SafeAreaView,
  Image,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {TouchableOpacity} from 'react-native-gesture-handler';




export default function WelcomeScreen() {
  const navigation = useNavigation<StackNavigationProp<any>>();

  return (
      <>
          <View style={styles.body}>
              <ImageBackground
                  style={styles.background}
                  source={require('../images/green-bananas-background.jpg')}>
                  <View style={styles.container}>
                      <View style={styles.loginTextContainer}>
                          <Image
                              style={styles.logo}
                              source={require('../icons/bananas-128.png')}
                          />
                          <Text style={styles.welcomeText}>Welcome to Bananar</Text>
                          <Text style={styles.appIntroText}>實時香蕉辨認器</Text>
                          <Text style={styles.appIntroText}>A real time banana ripeness classifier</Text>
                      </View>


                      <View style={styles.introButtonContainer}>
                                <TouchableOpacity
                                    style={styles.introButton}
                                    onPress={() => navigation.navigate('Intro')}>
                                    <Text style={styles.introButtonText}>香蕉成熟度指引</Text>
                                    <Text style={styles.introButtonText}>Banana Ripeness Chart</Text>
                                </TouchableOpacity>
                          </View>
                      
                          <View style={styles.camTextAndButtonContainer}>
                            <Text style={styles.camText}>請將你的鏡頭對準香蕉</Text>
                            <Text style={styles.camText}>Please focus your camera on banana only</Text>
                                <TouchableOpacity
                                    style={styles.camButton}
                                    onPress={() => navigation.navigate('Camera')}>
                                    <Image
                                        style={styles.camLogo}
                                        source={require('../icons/camera-64.png')}
                                    />
                                    <Text style={styles.buttonText}>Start 開始</Text>
                                </TouchableOpacity>
                          </View>
                      
                  </View>
              </ImageBackground>
          </View>
      </>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  background: {
    flex: 1,
  },
  container: {
    flex: 1,
    width: '100%',
  },
  loginTextContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 100,
    height: 100,
    marginBottom: 5,
  },
  welcomeText: {
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: 'white',
    fontSize: 28,
    textShadowColor:'#000000',
    textShadowOffset:{width: 2, height: 2},
    textShadowRadius:1,
    marginBottom: 10
  },
  appIntroText: {
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: 'white',
    fontSize: 16,
    textShadowColor:'#000000',
    textShadowOffset:{width: 2, height: 2},
    textShadowRadius:1,
  }
  ,
  introButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 0,
  },
  introButton: {
    width: 200,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ee8727',
    height: 45,
    borderRadius: 4,
    shadowColor: 'black',
    shadowOpacity: 0.8,
    elevation: 8,
    shadowRadius: 10,
    shadowOffset: {width: 5, height: 10},
    marginTop: 10
  },
  introButtonText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 13,
  },
  camTextAndButtonContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 0,
  },
  camText: {
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: 'white',
    fontSize: 14,
    textShadowColor:'#000000',
    textShadowOffset:{width: 1, height: 1},
    textShadowRadius:1,
  },
  camLogo: {
    width: 50,
    height: 50,
    marginBottom: 5,
  },
  buttonText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 20,
  },
  camButton: {
    width: 120,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f9bb11',
    height: 120,
    borderRadius: 4,
    shadowColor: 'black',
    shadowOpacity: 0.8,
    elevation: 8,
    shadowRadius: 10,
    shadowOffset: {width: 5, height: 10},
    marginTop: 10
  },
});
