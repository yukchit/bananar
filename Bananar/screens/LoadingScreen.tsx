import React, {useEffect} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {useNavigation, StackActions} from '@react-navigation/native';
import {DotIndicator} from 'react-native-indicators';
import { StackNavigationProp } from '@react-navigation/stack';

export default function LoadingScreen() {

  const navigation = useNavigation();

    useEffect(() => {
      setTimeout(() => navigation.dispatch(StackActions.replace("Welcome")), 3000)
      }, []);

  
  return (
    <View style={styles.body}>
      <View style={styles.safeAreaViewContainer}>
        <Image
          style={styles.logo}
          source={require('../icons/bananas-128.png')}
        />
        <View style={styles.loadingIconContainer}>
          <DotIndicator color="white" size={15} />
        </View>
        <Text style={styles.loadingText}>LOADING...</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  body: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: '#8fc400',
  },
  safeAreaViewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  logo: {
    width: 100,
    height: 100,
    marginBottom: 5,
  },
  loadingIconContainer: {
    marginTop: 15,
    height: 15,
    display: 'flex',
    justifyContent: 'flex-end',
  },
  loadingText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 15,
  },
});
