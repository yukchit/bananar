import React from 'react';
import {Image, View, StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import LoadingScreen from './screens/LoadingScreen';
import CameraScreen from './screens/CameraScreen';
import WelcomeScreen from './screens/WelcomeScreen';
import IntroScreen from './screens/IntroScreen';
import {TouchableOpacity} from 'react-native-gesture-handler';

const BananarStack = createStackNavigator();
function BananarStackScreen() {
  return (
    <>
      <BananarStack.Navigator>
        <BananarStack.Screen
          name="Loading"
          component={LoadingScreen}
          options={{headerShown: false, gestureEnabled: false}}
        />
        <BananarStack.Screen
          name="Welcome"
          component={WelcomeScreen}
          options={{headerShown: false, gestureEnabled: false}}
        />
        <BananarStack.Screen
          name="Intro"
          component={IntroScreen}
          options={{headerShown: false, gestureEnabled: false}}
        />
        <BananarStack.Screen
          name="Camera"
          component={CameraScreen}
          options={{headerShown: false, gestureEnabled: false}}
        />
        
      </BananarStack.Navigator>
    </>
  );
}


const Stack = createStackNavigator();
const App = () => {
  return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Root"
            component={BananarStackScreen}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
  );
};


export default App;
